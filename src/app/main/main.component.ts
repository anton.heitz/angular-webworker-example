import { Component, OnInit } from '@angular/core';
import { createWorkbook, blockThreadForSeconds } from '../utils/payload';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  countTo: number = 8000000;
  timePassed: number = 0;
  result : number = 0;
  counter: number = 0;
  message: number = 0;
  maxCount: number = 5;

  callTestSimple() {
    var t = Date.now();
    createWorkbook(this.countTo).then(result => {
      this.saveBlob(result, t)
    })
  }

  callTestWorker() {
    var t = Date.now();

    if (typeof Worker !== 'undefined') {
      // Create a new
      const worker = new Worker(new URL('./main.worker', import.meta.url));

      worker.onmessage = ({ data }) => {
        this.saveBlob(data, t)
      };

      worker.postMessage({ playload: this.countTo });

    } else {

      // Web Workers are not supported in this environment.
      // You should add a fallback so that your program still executes correctly.
    }

  }

  saveBlob(blob: Blob, start: number) {
    saveAs(blob, "text.xlsx")
    this.timePassed = Date.now() - start;
  }

  increaseCounter() {
    this.counter++;
  }

  callUpdatesSimple() {
    this.message = 0;
    for (var i = 0; i < this.maxCount; i++) {
      blockThreadForSeconds(1, () => {
        this.message = i + 1;
      })
    }
  }

  callUpdatesWorker() {

    if (typeof Worker !== 'undefined') {
      // Create a new
      const worker = new Worker(new URL('./updates.worker', import.meta.url));

      worker.onmessage = ({ data }) => {
        this.message = data
      };

      worker.postMessage({ max: this.maxCount });

    } else {

      // Web Workers are not supported in this environment.
      // You should add a fallback so that your program still executes correctly.
    }

  }
}


