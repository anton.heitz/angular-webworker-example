/// <reference lib="webworker" />
import { createWorkbook } from '../utils/payload';

addEventListener('message', ({ data }) => {
  
  createWorkbook(data.playload).then(blob => {
    postMessage(blob)
  })

});
