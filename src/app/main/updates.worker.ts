/// <reference lib="webworker" />
import { blockThreadForSeconds } from '../utils/payload';

addEventListener('message', ({ data }) => {

  postMessage(0)

  for (var i = 0; i < data.max; i++) {
    
    blockThreadForSeconds(1, () => {
      postMessage(i + 1)
    })

  }

});
