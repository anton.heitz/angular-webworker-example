import * as ExcelJS from 'exceljs/dist/exceljs.min.js';

export function createWorkbook(max: number): Promise<Blob> {

    return new Promise<Blob>((resolve, reject) => {
        const wb: ExcelJS.Workbook = new ExcelJS.Workbook();

        const textWS = wb.addWorksheet("test");

        for (var i = 0; i < max; i++) {
            textWS.addRow(i)
        }

        wb.xlsx.writeBuffer().then((data: any) => {
            let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' })
            resolve(blob)
        })
    })

}

export function blockThreadForSeconds(seconds: number, cb: Function): void {
    var t_s = new Date().getTime();
    var t_e = t_s;
    while (t_e < t_s + (seconds * 1000)) {
        t_e = new Date().getTime();
    }
    cb()
}